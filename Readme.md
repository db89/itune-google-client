## Multi API Client request examples

#### Instructions for running

`mvn clean install`

`java -jar target/test-1.0-SNAPSHOT.jar`

`config.properties` located in resources contains the search limits for individual api clients.

#### Example

`curl -H "Content-Type: application/json" -X POST -d '{"term":"test"}' http://localhost:9000/api/v1/search`

response is 

```
{"resultsFound":10,"searchResults":[{"name":"Test","type":"album","creator":"Little Dragon"},{"name":"Test","type":"album","creator":"Ministry"},{"name":"Mmm Mmm Mmm Mmm","type":"album","creator":"Crash Test Dummies"},{"name":"Test","type":"album","creator":"Chris Mason Johnson"},{"name":"Walt Grace's Submarine Test, January 1967","type":"album","creator":"John Mayer"},{"name":"How Google Tests Software","type":"book","creator":"James A. Whittaker ,Jason Arbon ,Jeff Carollo"},{"name":"Language Test Construction and Evaluation","type":"book","creator":"J. Charles Alderson ,Caroline Clapham ,Dianne Wall"},{"name":"Barron's Mechanical Aptitude and Spatial Relations Test","type":"book","creator":"Joel Wiesen"},{"name":"Satire","type":"book","creator":"George Austin Test"},{"name":"Category-Specific Names Test","type":"book","creator":"Pat McKenna"}]}

```

#### Explanation

I have wrapped the request to downstream services in hysterix because

1. We use the fallback in a scenario where the downstream services are not available
2. We have limit the execution time in a scenario where one of the downstream service is flaky
3. We can make multiple client request asynchronously.

I have spring boot

1. Easy to implement
2. Actuators provide metrics and health endpoint
3. We can add custom counters, metrics and health data to acutators.


If you want to explore the code, starting point would be the `SearchController`.

The mechanism for calling external api reside in `SearchServiceImpl`. 
Essential we execute the api calls asynchronously, 
We wait till all the observables are terminated and then collect and transform into api response.


#### Custom Metrics and Health

Endpoint for metrics - `http://localhost:9001/metrics`
Endpoint for health - `http://localhost:9001/health`

`counter.api.request`

`gauge.api.responsetime.curr`

`gauge.api.responsetime.avg`

`gauge.api.downstream.responsetime.avg.google_books`

`gauge.api.downstream.responsetime.curr.google_books`

`gauge.api.downstream.responsetime.avg.itunes`

`gauge.api.downstream.responsetime.curr.itunes`

Health endpoints provide status of ituneApi and googleBooksApi.

By default spring boot doesnt provide avg response times, 
there I created a metrics wrapper where last 100 response times are collected and average to obtain the information.

