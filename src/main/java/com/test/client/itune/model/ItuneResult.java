package com.test.client.itune.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dbasak on 01/12/17.
 */
public class ItuneResult {

    @JsonProperty
    private String trackName;

    @JsonProperty
    private String artistName;

    @JsonProperty
    private String trackViewUrl;

    @JsonProperty
    private String artworkUrl30;

    public String getTrackName() {
        return trackName;
    }

    public String getTrackViewUrl() {
        return trackViewUrl;
    }

    public String getArtworkUrl30() {
        return artworkUrl30;
    }

    public String getArtistName() {
        return artistName;
    }
}
