package com.test.client.itune.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by dbasak on 01/12/17.
 */

public class ItuneModel {

    @JsonProperty
    private int resultCount;

    @JsonProperty
    private List<ItuneResult> results;

    public int getResultCount() {
        return resultCount;
    }

    public List<ItuneResult> getResults() {
        return results;
    }
}
