package com.test.client.itune;

import com.netflix.config.DynamicPropertyFactory;
import com.test.client.ApiClient;
import com.test.http.ApiRequest;
import com.test.http.ApiRequestBuilder;
import com.test.metrics.ServiceMetric;
import com.test.http.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rx.Observable;

import static com.test.http.AsyncClientCommand.fetch;

/**
 * Created by dbasak on 01/12/17.
 */
@Component
public class ItunesApiClient implements ApiClient {

    private static final String API_TAG = "itunes";
    private static final String ITUNES = "https://itunes.apple.com/search?term=%s&limit=%s";
    private static final String ITUNE_SEARCH_LIMIT = "itune.search.limit";
    private final ServiceMetric serviceMetrics;
    private ItunesResponseHandler ituneResponseHandler;
    private static final Logger logger = LoggerFactory.getLogger(ItunesApiClient.class);

    @Autowired
    public ItunesApiClient(ItunesResponseHandler ituneResponseHandler, ServiceMetric serviceMetrics) {
        this.ituneResponseHandler = ituneResponseHandler;
        this.serviceMetrics = serviceMetrics;
    }

    @Override
    public Observable<ApiResponse> apply(String searchTerm) {

        int searchLimit = DynamicPropertyFactory.getInstance()
                .getIntProperty(ITUNE_SEARCH_LIMIT, 5)
                .get();

        logger.info("Current limit for itune is {} for search term {}", searchLimit, searchTerm);

        ApiRequest apiRequest = ApiRequestBuilder.create()
                .setMetrics(serviceMetrics)
                .setLimit(searchLimit)
                .setSearchTerm(searchTerm)
                .setApiTag(API_TAG)
                .setUrl(ITUNES)
                .setResponseHandler(this.ituneResponseHandler)
                .build();

        return fetch(apiRequest).observe();
    }
}
