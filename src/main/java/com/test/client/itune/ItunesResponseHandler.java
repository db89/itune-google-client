package com.test.client.itune;

import com.google.gson.Gson;
import com.test.client.itune.model.ItuneModel;
import com.test.http.ApiResponse;
import com.test.http.SearchResult;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by dbasak on 01/12/17.
 */
@Component
public class ItunesResponseHandler implements ResponseHandler<Optional<ApiResponse>> {

    private static final String ITUNES = "album";
    private Gson gson;

    @Autowired
    public ItunesResponseHandler(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Optional<ApiResponse> handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
        int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                List<SearchResult> collected = gson
                        .fromJson(EntityUtils.toString(entity), ItuneModel.class)
                        .getResults()
                        .stream()
                        .filter(s -> !s.getTrackName().isEmpty())
                        .map(s -> SearchResult.SearchResultBuilder
                                .init()
                                .addImage(s.getArtworkUrl30())
                                .addResourceName(s.getTrackName())
                                .addArtistAuthor(s.getArtistName())
                                .addType(ITUNES)
                                .addUrl(s.getTrackViewUrl())
                                .build())
                        .collect(Collectors.toList());

                return Optional.of(new ApiResponse(collected.size(), collected));
            } else {
                return Optional.empty();
            }
        } else {
            throw new ClientProtocolException("Unexpected response status: " + status);
        }
    }
}