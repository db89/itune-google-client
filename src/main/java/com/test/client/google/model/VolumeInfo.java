package com.test.client.google.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dbasak on 01/12/17.
 */
public class VolumeInfo {

    @JsonProperty
    private String title;

    @JsonProperty
    private String[] authors;

    @JsonProperty
    private String previewLink;

    @JsonProperty
    private ImageLink imageLinks;

    public String getTitle() {
        return title;
    }

    public ImageLink getImageLinks() {
        return imageLinks;
    }

    public String getPreviewLink() {
        return previewLink;
    }

    public String[] getAuthors() {
        return authors;
    }
}
