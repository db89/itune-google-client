package com.test.client.google.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by dbasak on 01/12/17.
 */
public class GoogleBookModel {

    @JsonProperty
    private int totalItems;

    @JsonProperty
    private List<GoogleBookItems> items;

    public int getTotalItems() {
        return totalItems;
    }

    public List<GoogleBookItems> getItems() {
        return items;
    }
}
