package com.test.client.google.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dbasak on 01/12/17.
 */
public class ImageLink {

    @JsonProperty
    private String smallThumbnail;

    public String getSmallThumbnail() {
        return smallThumbnail;
    }
}
