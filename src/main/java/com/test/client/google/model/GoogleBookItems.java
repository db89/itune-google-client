package com.test.client.google.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dbasak on 01/12/17.
 */
public class GoogleBookItems {

    @JsonProperty
    private VolumeInfo volumeInfo;

    public VolumeInfo getVolumeInfo() {
        return volumeInfo;
    }
}
