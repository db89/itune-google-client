package com.test.client.google;

import com.google.gson.Gson;
import com.test.client.google.model.GoogleBookItems;
import com.test.client.google.model.GoogleBookModel;
import com.test.http.ApiResponse;
import com.test.http.SearchResult;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class GooglebooksResponseHandler implements ResponseHandler<Optional<ApiResponse>> {

    public static final String GOOGLE_BOOKS = "book";
    private Gson gson;

    public GooglebooksResponseHandler(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Optional<ApiResponse> handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
        int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                List<SearchResult> collected = gson
                        .fromJson(EntityUtils.toString(entity), GoogleBookModel.class)
                        .getItems()
                        .stream()
                        .map(GoogleBookItems::getVolumeInfo)
                        .filter(s -> !s.getTitle().isEmpty())
                        .map(s -> SearchResult.SearchResultBuilder
                                .init()
                                .addImage(s.getImageLinks().getSmallThumbnail())
                                .addResourceName(s.getTitle())
                                .addArtistAuthor(Arrays
                                        .stream(s.getAuthors())
                                        .reduce( (authors, author) -> authors + " ," +author)
                                        .orElseGet(() -> "No authors found"))
                                .addType(GOOGLE_BOOKS)
                                .addUrl(s.getPreviewLink())
                                .build()).collect(Collectors.toList());
                return Optional.of(new ApiResponse(collected.size(), collected));
            } else {
                return Optional.empty();
            }
        } else {
            throw new ClientProtocolException("Unexpected response status: " + status);
        }
    }
}
