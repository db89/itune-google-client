package com.test.client.google;

import com.netflix.config.DynamicPropertyFactory;
import com.test.client.ApiClient;
import com.test.http.ApiRequest;
import com.test.http.ApiRequestBuilder;
import com.test.http.ApiResponse;
import com.test.metrics.ServiceMetric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rx.Observable;

import static com.test.http.AsyncClientCommand.fetch;

/**
 * Created by dbasak on 01/12/17.
 */
@Component
public class GoogleBookApiClient implements ApiClient {

    private static final String API_TAG = "google_books";
    private static final String GOOGLE_BOOKS = "https://www.googleapis.com/books/v1/volumes?q=%s&maxResults=%s";
    private static final String GOOGLE_SEARCH_LIMIT = "google.search.limit";
    private GooglebooksResponseHandler googlebooksResponseHandler;
    private ServiceMetric serviceMetric;
    private static final Logger logger = LoggerFactory.getLogger(GoogleBookApiClient.class);

    @Autowired
    public GoogleBookApiClient(GooglebooksResponseHandler googlebooksResponseHandler, ServiceMetric serviceMetric) {
        this.googlebooksResponseHandler = googlebooksResponseHandler;
        this.serviceMetric = serviceMetric;
    }

    @Override
    public Observable<ApiResponse> apply(String searchTerm) {

        int searchLimit = DynamicPropertyFactory.getInstance()
                .getIntProperty(GOOGLE_SEARCH_LIMIT, 5)
                .get();

        logger.info("Current search limit for google is {} for searchTerm {}", searchLimit, searchTerm);

        ApiRequest apiRequest = ApiRequestBuilder
                .create()
                .setApiTag(API_TAG)
                .setUrl(GOOGLE_BOOKS)
                .setMetrics(serviceMetric)
                .setSearchTerm(searchTerm)
                .setResponseHandler(this.googlebooksResponseHandler)
                .setLimit(searchLimit)
                .build();

        return fetch(apiRequest).observe();
    }
}
