package com.test.client.google.health;

import com.test.client.google.GoogleBookApiClient;
import com.test.http.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * Created by dbasak on 03/12/17.
 */
@Component
public class GoogleApiHealthIndicator implements HealthIndicator {

    private GoogleBookApiClient googleBookApiClient;

    @Autowired
    public GoogleApiHealthIndicator(GoogleBookApiClient googleBookApiClient) {
        this.googleBookApiClient = googleBookApiClient;
    }

    @Override
    public Health health() {
        int errorCode = check(); // perform some specific health check
        if (errorCode != 0) {
            return Health.down().withDetail("Error Code", errorCode).build();
        }
        return Health.up().build();
    }

    /**
     * Request with pre configured values to test if service is up
     * @return
     */
    private int check(){
        ApiResponse test = this.googleBookApiClient.apply("test").toBlocking().single();
        if(test.getResultsFound() != 0){
            return 0;
        } else {
            return 404;
        }
    }
}
