package com.test.client;

import com.test.http.ApiResponse;
import rx.Observable;

/**
 * Created by dbasak on 01/12/17.
 */
@FunctionalInterface
public interface ApiClient {
    Observable<ApiResponse> apply(String searchTerm);
}
