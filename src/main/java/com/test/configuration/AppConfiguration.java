package com.test.configuration;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * Created by dbasak on 03/12/17.
 */
@Configuration
public class AppConfiguration {

    @Bean
    public Cache<String, List<Long>> createMetricsCache(){
        return CacheBuilder.newBuilder()
                .maximumSize(100)
                .build();
    }
}
