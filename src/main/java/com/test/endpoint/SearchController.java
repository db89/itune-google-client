package com.test.endpoint;

import com.test.http.ApiResponse;
import com.test.http.SearchRequest;
import com.test.service.GaugeSearchServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

/**
 * Created by dbasak
 */
@RestController
@EnableAutoConfiguration
public class SearchController {

    private GaugeSearchServiceImpl gaugedSearchService;

    @Autowired
    public SearchController(GaugeSearchServiceImpl gaugeSearchService) {
        this.gaugedSearchService = gaugeSearchService;
    }

    @RequestMapping(value = "/api/v1/search", method = RequestMethod.POST)
    @ResponseBody
    public ApiResponse index(@RequestBody SearchRequest searchTerm) {
        return this.gaugedSearchService.search(searchTerm.getTerm());
    }
}
