package com.test.http;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.test.metrics.ServiceMetric;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by dbasak
 */
public class AsyncClientCommand extends HystrixCommand<ApiResponse> {
    private static final String GAUGE_AVG = "gauge.api.downstream.responsetime.avg.%s";
    private static final String GAUGE_CURR = "gauge.api.downstream.responsetime.curr.%s";
    private static final Logger logger = LoggerFactory.getLogger(AsyncClientCommand.class);
    private final String url;
    private final ResponseHandler<Optional<ApiResponse>> responseHandler;
    private ServiceMetric serviceMetric;
    private String apiTag;

    AsyncClientCommand(final String url,
                       String apiTag,
                       final ResponseHandler<Optional<ApiResponse>> responseHandler,
                       ServiceMetric serviceMetric) {

        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("HTTP"))
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                        .withExecutionTimeoutInMilliseconds(20000)));
        this.url = url;
        this.apiTag = apiTag;
        this.responseHandler = responseHandler;
        this.serviceMetric = serviceMetric;
    }

    public static AsyncClientCommand fetch(ApiRequest apiRequest) {
        return new AsyncClientCommand(String.format(apiRequest.getUrl(), apiRequest.getSearchTerm(), apiRequest.getLimit()),
                apiRequest.getApiTag(),
                apiRequest.getResponseHandler(),
                apiRequest.getServiceMetric());
    }

    @Override
    protected ApiResponse run() throws Exception {
        try {

        long startTime = System.currentTimeMillis();

        CloseableHttpClient aDefault = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(url);
        Optional<ApiResponse> execute = aDefault.execute(httpget, responseHandler);

        ApiResponse apiResponse = execute.orElseGet(() -> new ApiResponse(0, new ArrayList<>()));

        long endTime = System.currentTimeMillis();

        long timeTaken = endTime - startTime;
        this.serviceMetric.avgResponseTime(String.format(GAUGE_AVG, this.apiTag), timeTaken);
        this.serviceMetric.submit(String.format(GAUGE_CURR, this.apiTag), timeTaken);

        logger.info("processed {}, timeTaken {}ms", url, timeTaken);
        return apiResponse;
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    protected ApiResponse getFallback() {
        return new ApiResponse(0, new ArrayList<>());
    }
}