package com.test.http;

import com.test.metrics.ServiceMetric;
import org.apache.http.client.ResponseHandler;

import java.util.Optional;

/**
 * Created by dbasak on 02/12/17.
 */
public class ApiRequestBuilder {
    private String url;
    private String apiTag;
    private String searchTerm;
    private int limit;
    private ResponseHandler<Optional<ApiResponse>> responseHandler;
    private ServiceMetric serviceMetric;

    public static ApiRequestBuilder create() {
        return new ApiRequestBuilder();
    }

    public ApiRequestBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public ApiRequestBuilder setApiTag(String apiTag) {
        this.apiTag = apiTag;
        return this;
    }

    public ApiRequestBuilder setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
        return this;
    }

    public ApiRequestBuilder setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    public ApiRequestBuilder setResponseHandler(ResponseHandler<Optional<ApiResponse>> responseHandler) {
        this.responseHandler = responseHandler;
        return this;
    }

    public ApiRequestBuilder setMetrics(ServiceMetric serviceMetric) {
        this.serviceMetric = serviceMetric;
        return this;
    }

    public ApiRequest build() {
        return new ApiRequest(url, apiTag, searchTerm, limit, responseHandler, serviceMetric);
    }
}
