package com.test.http;

import java.util.List;

/**
 * Created by dbasak on 01/12/17.
 */
public class ApiResponse {

    private int resultsFound;
    private List<SearchResult> searchResults;

    public ApiResponse(int status, List<SearchResult> searchResults) {
        this.resultsFound = status;
        this.searchResults = searchResults;
    }

    public ApiResponse() {
        //required for jackson parsing
    }

    public int getResultsFound() {
        return resultsFound;
    }

    public List<SearchResult> getSearchResults() {
        return searchResults;
    }
}
