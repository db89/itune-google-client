package com.test.http;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dbasak
 */
public class SearchResult {

    @JsonProperty("name")
    private String resourceName;
    @JsonProperty("type")
    private String resourceType;
    @JsonProperty("creator")
    private String artistAuthorName;

    public SearchResult(String resourceName,
                        String resourceType,
                        String image,
                        String resourceUrl,
                        String artistAuthorName) {
        this.resourceName = resourceName;
        this.resourceType = resourceType;
        this.artistAuthorName = artistAuthorName;
    }

    public SearchResult() {
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getResourceType() {
        return resourceType;
    }

    public String getArtistAuthorName() {
        return artistAuthorName;
    }

    public static class SearchResultBuilder {

        private String resourceType;
        private String resourceName;
        private String resourceUrl;
        private String resourceImage;
        private String artistAuthorName;

        public static SearchResultBuilder init() {
            return new SearchResultBuilder();
        }

        public SearchResultBuilder addType(String resourceType) {
            this.resourceType = resourceType;
            return this;
        }

        public SearchResultBuilder addResourceName(String resourceName) {
            this.resourceName = resourceName;
            return this;
        }

        public SearchResultBuilder addUrl(String resourceUrl) {
            this.resourceUrl = resourceUrl;
            return this;
        }

        public SearchResultBuilder addImage(String resourceImage) {
            this.resourceImage = resourceImage;
            return this;
        }

        public SearchResultBuilder addArtistAuthor(String artistAuthorName) {
            this.artistAuthorName = artistAuthorName;
            return this;
        }

        public SearchResult build() {
            return new SearchResult(this.resourceName, this.resourceType, this.resourceImage, this.resourceUrl, this.artistAuthorName);
        }
    }

}

