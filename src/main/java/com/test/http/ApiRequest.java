package com.test.http;

import com.test.metrics.ServiceMetric;
import org.apache.http.client.ResponseHandler;

import java.util.Optional;

/**
 * Created by dbasak on 02/12/17.
 */
public class ApiRequest {
    private String url;
    private String apiTag;
    private String searchTerm;
    private int limit;
    private ResponseHandler<Optional<ApiResponse>> responseHandler;
    private ServiceMetric serviceMetric;

    public ApiRequest(String url,
                      String apiTag,
                      String searchTerm,
                      int limit, ResponseHandler<Optional<ApiResponse>> responseHandler,
                      ServiceMetric serviceMetric) {
        this.url = url;
        this.apiTag = apiTag;
        this.searchTerm = searchTerm;
        this.limit = limit;
        this.responseHandler = responseHandler;
        this.serviceMetric = serviceMetric;
    }

    public String getUrl() {
        return url;
    }

    public String getApiTag() {
        return apiTag;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public int getLimit() {
        return limit;
    }

    public ResponseHandler<Optional<ApiResponse>> getResponseHandler() {
        return responseHandler;
    }

    public ServiceMetric getServiceMetric() {
        return serviceMetric;
    }
}
