package com.test.http;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dbasak on 01/12/17.
 */
public class SearchRequest {

    @JsonProperty
    private String term;

    public SearchRequest() {
        //ignore
    }

    public SearchRequest(String term) {
        this.term = term;
    }

    public String getTerm() {
        return term;
    }
}

