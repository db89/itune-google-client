package com.test.metrics;

import org.springframework.stereotype.Component;

/**
 * Created by dbasak on 03/12/17.
 */
@Component
public interface ServiceMetric {
    public void submit(final String tag, final long timeTaken);
    public void avgResponseTime(final String tag, final long timeTaken);
    public void increment(String tag);
}
