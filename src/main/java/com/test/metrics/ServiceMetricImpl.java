package com.test.metrics;

import com.google.common.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dbasak on 03/12/17.
 */

@Component
public class ServiceMetricImpl implements ServiceMetric {

    private final CounterService counterService;
    private final GaugeService gaugeService;
    private final Cache<String, List<Long>> cache;

    @Autowired
    public ServiceMetricImpl(CounterService counterService,
                             GaugeService gaugeService,
                             Cache<String, List<Long>> cache) {
        this.counterService = counterService;
        this.gaugeService = gaugeService;
        this.cache = cache;
    }

    public void submit(final String tag, final long timeTaken) {

        List<Long> searchServiceMetrics = this.cache.getIfPresent(tag);

        if (searchServiceMetrics != null) {
            searchServiceMetrics.add(timeTaken);
        } else {
            searchServiceMetrics = new ArrayList<>();
            searchServiceMetrics.add(timeTaken);
        }

        searchServiceMetrics
                .stream()
                .mapToDouble(a -> a)
                .average()
                .ifPresent(value -> this.gaugeService.submit(tag, value));
    }

    public void avgResponseTime(final String tag, final long timeTaken){

        List<Long> searchServiceMetrics = this.cache.getIfPresent(tag);

        if (searchServiceMetrics != null) {
            searchServiceMetrics.add(timeTaken);
        } else {
            searchServiceMetrics = new ArrayList<>();
            searchServiceMetrics.add(timeTaken);
        }

        searchServiceMetrics
                .stream()
                .mapToDouble(a -> a)
                .average()
                .ifPresent(value -> this.gaugeService.submit(tag, value));
    }

    public void increment(String tag){
        this.counterService.increment(tag);
    }
}
