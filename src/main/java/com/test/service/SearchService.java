package com.test.service;


import com.test.http.ApiResponse;
import org.springframework.stereotype.Service;

/**
 * Created by dbasak on 02/12/17.
 */
@FunctionalInterface
@Service
public interface SearchService {
    public ApiResponse search(String searchTerm);
}
