package com.test.service;

import com.google.common.cache.Cache;
import com.test.metrics.ServiceMetric;
import com.test.http.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dbasak on 02/12/17.
 */
@Service
public class GaugeSearchServiceImpl implements SearchService {

    private static final Logger logger = LoggerFactory.getLogger(GaugeSearchServiceImpl.class);
    private static final String API_REQUEST_COUNT = "counter.api.request";
    private static final String SEARCH_CURR = "gauge.api.responsetime.avg";
    private static final String SEARCH_AVG = "gauge.api.responsetime.curr";
    private ServiceMetric serviceMetrics;
    private SearchServiceImpl searchService;

    @Autowired
    public GaugeSearchServiceImpl(ServiceMetric serviceMetrics,
                                  SearchServiceImpl searchService) {
        this.serviceMetrics = serviceMetrics;
        this.searchService = searchService;
    }

    @Override
    public ApiResponse search(String searchTerm) {

        long startTime = System.currentTimeMillis();

        ApiResponse search = this.searchService.search(searchTerm);

        long endTime = System.currentTimeMillis();
        long timeTaken = endTime - startTime;

        this.serviceMetrics.increment(API_REQUEST_COUNT);
        this.serviceMetrics.avgResponseTime(SEARCH_AVG, timeTaken);
        this.serviceMetrics.submit(SEARCH_CURR, timeTaken);

        logger.info("searchterm={} with resultsize={}", searchTerm, search.getResultsFound());

        return search;
    }
}
