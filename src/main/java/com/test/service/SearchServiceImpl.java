package com.test.service;

import com.test.client.ApiClient;
import com.test.http.ApiResponse;
import com.test.http.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;
import rx.schedulers.Schedulers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.net.URLEncoder.encode;

/**
 * Created by dbasak on 02/12/17.
 */
@Service
public class SearchServiceImpl {

    private List<ApiClient> apiClients;
    private static final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);

    @Autowired
    public SearchServiceImpl(List<ApiClient> apiClients) {
        this.apiClients = apiClients;
    }

    /***
     * Takes a search term and limitPerApi per api and return a response from all the underlying api clients.
     * The method waits till all the api calls are completed.
     * @param searchTerm
     * @return
     */
    public ApiResponse search(final String searchTerm) {

        logger.info("Search term requested {}", searchTerm);

        String searchQuery;
        try {
            searchQuery = encode(searchTerm,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("error while encoding search term - {}", e.getLocalizedMessage());
            return new ApiResponse(0, new ArrayList<>());
        }

        List<ApiResponse> responses = new ArrayList<>();

        List<Observable<ApiResponse>> ap = this.apiClients
                .stream()
                .map(apiClient -> apiClient.apply(searchQuery))
                .collect(Collectors.toList());

        Observable.from(ap)
                .flatMap(task -> task.observeOn(Schedulers.computation()))
                .map(res -> res)
                .toBlocking().getIterator()
                .forEachRemaining(responses::add);

        List<SearchResult> collect = responses.stream()
                .flatMap(a -> a.getSearchResults().stream())
                .map(a -> a)
                .collect(Collectors.toList());

        return new ApiResponse(collect.size(), collect);
    }
}
