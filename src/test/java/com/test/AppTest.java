package com.test;

import com.test.http.ApiResponse;
import com.test.http.SearchRequest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by dbasak on 01/12/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AppTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void exampleTest() {
        ApiResponse body = this.restTemplate.postForObject("/api/v1/search", new SearchRequest("term"), ApiResponse.class);

        Assert.assertTrue(body.getSearchResults().size() > 0);
        Assert.assertTrue(body.getResultsFound() > 0);

        body.getSearchResults().forEach(res -> {
            Assert.assertTrue(!res.getResourceName().isEmpty());
            Assert.assertTrue(!res.getResourceType().isEmpty());
            Assert.assertTrue(!res.getArtistAuthorName().isEmpty());
        });

    }
}
