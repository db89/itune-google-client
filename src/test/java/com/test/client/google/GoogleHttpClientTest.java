package com.test.client.google;

import com.google.gson.Gson;
import com.test.client.MockServiceMetrics;
import com.test.http.ApiResponse;
import org.junit.Assert;
import org.junit.Test;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by dbasak on 01/12/17.
 */
public class GoogleHttpClientTest {

    @Test
    public void TestHttpClient() {

        GoogleBookApiClient googleBookApiClient = new GoogleBookApiClient(new GooglebooksResponseHandler(new Gson()), new MockServiceMetrics());
        Observable<ApiResponse> test = googleBookApiClient.apply("test");

        ApiResponse first = test.observeOn(Schedulers.computation()).toBlocking().first();

        Assert.assertTrue(first.getResultsFound() > 0);
        Assert.assertTrue(first.getSearchResults().size() > 0);
    }
}
