package com.test.client.google;

import com.google.gson.Gson;
import com.test.client.google.model.GoogleBookModel;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by dbasak on 01/12/17.
 */
public class GoogleParserTest {

    private static final String GOOGLE_TEST_DATA = "{\"kind\":\"books#volumes\",\"totalItems\":708,\"items\":[{\"kind\":\"books#volume\",\"id\":\"r6lQ-430NL0C\",\"etag\":\"hPZJw7chbyY\",\"selfLink\":\"https://www.googleapis.com/books/v1/volumes/r6lQ-430NL0C\",\"volumeInfo\":{\"title\":\"Tet!\",\"subtitle\":\"The Turning Point in the Vietnam War\",\"authors\":[\"Don Oberdorfer\"],\"publisher\":\"JHU Press\",\"publishedDate\":\"1971\",\"description\":\"Finalist for the 1971 National Book Award In early 1968, Communist forces in Vietnam launched a surprise offensive that targeted nearly every city, town, and major military base throughout South Vietnam. For several hours, the U.S. embassy in Saigon itself came under siege by Viet Cong soldiers. Militarily, the offensive was a failure, as the North Vietnamese Army and its guerrilla allies in the south suffered devastating losses. Politically, however, it proved to be a crucial turning point in America's involvement in Southeast Asia and public opinion of the war. In this classic work of military history and war reportage—long considered the definitive history of Tet and its aftermath—Don Oberdorfer moves back and forth between the war and the home front to document the lasting importance of this military action. Based on his own observations as a correspondent for the Washington Post and interviews with hundreds of people who were caught up in the struggle, Tet! remains an essential contribution to our understanding of the Vietnam War.\",\"industryIdentifiers\":[{\"type\":\"ISBN_10\",\"identifier\":\"0801867037\"},{\"type\":\"ISBN_13\",\"identifier\":\"9780801867033\"}],\"readingModes\":{\"text\":false,\"image\":true},\"pageCount\":385,\"printType\":\"BOOK\",\"categories\":[\"History\"],\"maturityRating\":\"NOT_MATURE\",\"allowAnonLogging\":false,\"contentVersion\":\"0.2.1.0.preview.1\",\"imageLinks\":{\"smallThumbnail\":\"http://books.google.com/books/content?id=r6lQ-430NL0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api\",\"thumbnail\":\"http://books.google.com/books/content?id=r6lQ-430NL0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api\"},\"language\":\"en\",\"previewLink\":\"http://books.google.nl/books?id=r6lQ-430NL0C&printsec=frontcover&dq=tet&hl=&cd=1&source=gbs_api\",\"infoLink\":\"http://books.google.nl/books?id=r6lQ-430NL0C&dq=tet&hl=&source=gbs_api\",\"canonicalVolumeLink\":\"https://books.google.com/books/about/Tet.html?hl=&id=r6lQ-430NL0C\"},\"saleInfo\":{\"country\":\"NL\",\"saleability\":\"NOT_FOR_SALE\",\"isEbook\":false},\"accessInfo\":{\"country\":\"NL\",\"viewability\":\"PARTIAL\",\"embeddable\":true,\"publicDomain\":false,\"textToSpeechPermission\":\"ALLOWED\",\"epub\":{\"isAvailable\":false},\"pdf\":{\"isAvailable\":false},\"webReaderLink\":\"http://play.google.com/books/reader?id=r6lQ-430NL0C&hl=&printsec=frontcover&source=gbs_api\",\"accessViewStatus\":\"SAMPLE\",\"quoteSharingAllowed\":false},\"searchInfo\":{\"textSnippet\":\"In this classic work of military history and war reportage—long considered the definitive history of Tet and its aftermath—Don Oberdorfer moves back and forth between the war and the home front to document the lasting importance of this ...\"}}]}";
    private static final String PREVIEW_LINK = "http://books.google.nl/books?id=r6lQ-430NL0C&printsec=frontcover&dq=tet&hl=&cd=1&source=gbs_api";
    private static final String TITLE = "Tet!";
    private static final String IMAGE = "http://books.google.com/books/content?id=r6lQ-430NL0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api";

    @Test
    public void TestGoogleParser() {

        GoogleBookModel googleBookModel = getGoogleBookModel();

        Assert.assertNotNull(googleBookModel);
        Assert.assertEquals(708, googleBookModel.getTotalItems());
        Assert.assertEquals(1, googleBookModel.getItems().size());
        Assert.assertEquals(PREVIEW_LINK, googleBookModel.getItems().get(0).getVolumeInfo().getPreviewLink());
        Assert.assertEquals(TITLE, googleBookModel.getItems().get(0).getVolumeInfo().getTitle());
        Assert.assertEquals(IMAGE, googleBookModel.getItems().get(0).getVolumeInfo().getImageLinks().getSmallThumbnail());
    }

    private GoogleBookModel getGoogleBookModel() {
        return new Gson().fromJson(GOOGLE_TEST_DATA, GoogleBookModel.class);
    }
}
