package com.test.client.itune;

import com.google.gson.Gson;
import com.test.client.itune.model.ItuneModel;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by dbasak on 01/12/17.
 */
public class ItuneParserTest {


    private static final String ITUNE_DATA = "\n" +
            "\n" +
            "\n" +
            "{\n" +
            " \"resultCount\":1,\n" +
            " \"results\": [\n" +
            "{\"wrapperType\":\"track\", \"kind\":\"song\", \"artistId\":258535972, \"collectionId\":258615649, \"trackId\":258618600, \"artistName\":\"Little Dragon\", \"collectionName\":\"Little Dragon\", \"trackName\":\"Test\", \"collectionCensoredName\":\"Little Dragon\", \"trackCensoredName\":\"Test\", \"artistViewUrl\":\"https://itunes.apple.com/us/artist/little-dragon/258535972?uo=4\", \"collectionViewUrl\":\"https://itunes.apple.com/us/album/test/258615649?i=258618600&uo=4\", \"trackViewUrl\":\"https://itunes.apple.com/us/album/test/258615649?i=258618600&uo=4\", \n" +
            "\"previewUrl\":\"https://audio-ssl.itunes.apple.com/apple-assets-us-std-000001/AudioPreview62/v4/e6/77/4e/e6774eeb-b2d3-f214-84db-38d06b53563f/mzaf_7017187472362467720.plus.aac.p.m4a\", \"artworkUrl30\":\"http://is4.mzstatic.com/image/thumb/Music/v4/73/aa/cd/73aacdee-0259-fda7-3e6f-28433c098b38/source/30x30bb.jpg\", \"artworkUrl60\":\"http://is4.mzstatic.com/image/thumb/Music/v4/73/aa/cd/73aacdee-0259-fda7-3e6f-28433c098b38/source/60x60bb.jpg\", \"artworkUrl100\":\"http://is4.mzstatic.com/image/thumb/Music/v4/73/aa/cd/73aacdee-0259-fda7-3e6f-28433c098b38/source/100x100bb.jpg\", \"collectionPrice\":9.99, \"trackPrice\":0.99, \"releaseDate\":\"2007-08-28T07:00:00Z\", \"collectionExplicitness\":\"notExplicit\", \"trackExplicitness\":\"notExplicit\", \"discCount\":1, \"discNumber\":1, \"trackCount\":12, \"trackNumber\":10, \"trackTimeMillis\":268040, \"country\":\"USA\", \"currency\":\"USD\", \"primaryGenreName\":\"Electronic\", \"isStreamable\":true}]\n" +
            "}";

    private static final String TRACK_VIEW = "https://itunes.apple.com/us/album/test/258615649?i=258618600&uo=4";
    private static final String TRACK_NAME = "Test";
    private static final String ARTWORK = "http://is4.mzstatic.com/image/thumb/Music/v4/73/aa/cd/73aacdee-0259-fda7-3e6f-28433c098b38/source/30x30bb.jpg";

    @Test
    public void TestItuneParser() {
        ItuneModel ituneModel = getItuneModel();
        Assert.assertEquals(1, ituneModel.getResultCount());
        Assert.assertEquals(1, ituneModel.getResults().size());
        Assert.assertEquals(ARTWORK, ituneModel.getResults().get(0).getArtworkUrl30());
        Assert.assertEquals(TRACK_NAME, ituneModel.getResults().get(0).getTrackName());
        Assert.assertEquals(TRACK_VIEW, ituneModel.getResults().get(0).getTrackViewUrl());
    }

    private ItuneModel getItuneModel() {
        return new Gson().fromJson(ITUNE_DATA, ItuneModel.class);
    }

}
