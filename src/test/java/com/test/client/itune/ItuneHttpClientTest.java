package com.test.client.itune;

import com.google.gson.Gson;
import com.test.client.MockServiceMetrics;
import com.test.http.ApiResponse;
import org.junit.Assert;
import org.junit.Test;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by dbasak on 02/12/17.
 */
public class ItuneHttpClientTest {

    @Test
    public void TestHttpClient() {

        ItunesApiClient ituneApiClient = new ItunesApiClient(new ItunesResponseHandler(new Gson()), new MockServiceMetrics());
        Observable<ApiResponse> test = ituneApiClient.apply("test");

        ApiResponse first = test.observeOn(Schedulers.computation()).toBlocking().first();

        Assert.assertTrue(first.getResultsFound() > 0);
        Assert.assertTrue(first.getSearchResults().size() > 0);
    }
}
