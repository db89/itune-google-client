package com.test.client;

import com.test.metrics.ServiceMetric;

/**
 * Created by dbasak on 02/12/17.
 */
public class MockServiceMetrics implements ServiceMetric {

    //Do nothing
    @Override
    public void submit(String tag, long timeTaken) {

    }

    @Override
    public void avgResponseTime(String tag, long timeTaken) {

    }

    @Override
    public void increment(String tag) {

    }
}